//
//  RatingControl.swift
//  MyPlaces
//
//  Created by Roman Kosinevskyi on 05.01.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {

    //MARK: public property
    var rating = 0.0 {
        didSet {
            updateButtonSelectionState()
        }
    }
    @IBInspectable var starSize: CGSize = CGSize(width: 44, height: 44) {
        didSet {
            setupButtons()
        }
    }
    @IBInspectable var starCount: Int = 5 {
        didSet {
            setupButtons()
        }
    }
    
    //MARK: private property
    private var ratingBtns = [UIButton]()
    
    //MARK: Initialization
    //for storyboard
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    
    //for code
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
    }
    
    //MARK: methods
    @objc func ratingBtnTapped(button: UIButton) {
        guard let index = ratingBtns.firstIndex(of: button) else { return }
        
        let selectedRating = Double(index + 1)
        
        if selectedRating == rating {
            rating = 0
        } else {
            rating = selectedRating
        }
    }
    
    //MARK: private methods
    private func updateButtonSelectionState() {
        for (index, button) in ratingBtns.enumerated() {
            button.isSelected = Double(index) < rating
        }
    }
    
    private func setupButtons() {
        
        for button in ratingBtns {
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        
        ratingBtns.removeAll()
        
        let bundle = Bundle(for: type(of: self))
        let emptyStar = UIImage(named: "emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named: "highlightedStar", in: bundle, compatibleWith: self.traitCollection)
        
        for _ in 0..<starCount {
            
            //create btn
            let button = UIButton()
            
            //btn images
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.selected, .highlighted])
            
            //action
            button.addTarget(self, action: #selector(ratingBtnTapped(button:)), for: .touchUpInside)
            //constraint btn
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            
            //add the btn to the stack
            addArrangedSubview(button)
            
            //add to the array
            ratingBtns.append(button)
        }
        
        updateButtonSelectionState()
    }
}
