//
//  NewPlaceViewController.swift
//  MyPlaces
//
//  Created by Roman Kosinevskyi on 03.01.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cosmos

class NewPlaceViewController: UITableViewController {

    //MARK: public
    var currentPlace: Place!
    
    //MARK: private
    private var imageIsChanged = false
    //private var currentRating = 0.0
    
    //MARK: IBOutlet
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeName: UITextField!
    @IBOutlet weak var placeLocation: UITextField!
    @IBOutlet weak var placeType: UITextField!
    @IBOutlet weak var cosmosView: CosmosView!
    
    //MARK: IBAction
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
        //self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delete last line undex stars
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        
        saveBtn.isEnabled = false
        placeName.addTarget(self, action: #selector(textFiledChanged), for: .editingChanged)
        
        setupEditScreen()
        
//        cosmosView.didTouchCosmos = { rating in
//            self.currentRating = rating
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let cameraImage = #imageLiteral(resourceName: "camera")
            let photoImage = #imageLiteral(resourceName: "photo")
            
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Camera", style: .default) { _ in
                self.chooseImagePicker(source: .camera)
            }
            
            camera.setValue(cameraImage, forKey: "image")
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let photo = UIAlertAction(title: "Photo", style: .default) { _ in
                self.chooseImagePicker(source: .photoLibrary)
            }
            
            photo.setValue(photoImage, forKey: "image")
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel)
            
            actionSheet.addAction(camera)
            actionSheet.addAction(photo)
            actionSheet.addAction(cancel)
            
            present(actionSheet, animated: true)
        } else {
            view.endEditing(true)
        }
    }
    
    func savePlace() {
        guard let image = imageIsChanged ? placeImage.image : UIImage(named: "imagePlaceholder") else { return }
        //print("currentRating \(currentRating)")
        let newPlace = Place(name: placeName.text!,
                             location: placeLocation.text,
                             type: placeType.text,
                             imageData: image.pngData(),
                             rating: cosmosView.rating)
                            //rating: currentRating)
        
        if currentPlace != nil {
            try! realm.write {
                currentPlace?.name = newPlace.name
                currentPlace?.location = newPlace.location
                currentPlace?.type = newPlace.type
                currentPlace?.imageData = newPlace.imageData
                currentPlace?.rating = newPlace.rating
                
                CloudManager.updateCloudData(place: currentPlace, with: image)
            }
        } else {
            CloudManager.saveDataToCloud(place: newPlace, with: image) { (recordID) in
                DispatchQueue.main.async {
                    try! realm.write {
                        newPlace.recordID = recordID
                    }
                }
            }
            StorageManager.saveObject(newPlace)
        }
    }
    
    private func setupEditScreen() {
        if currentPlace != nil {
            setupNavigationBar()
            imageIsChanged = true
            
            guard let data = currentPlace.imageData, let image = UIImage(data: data) else { return }
            
            placeImage.image = image
            placeImage.contentMode = .scaleAspectFill
            
            placeName.text = currentPlace.name
            placeLocation.text = currentPlace.location
            placeType.text = currentPlace.type
            cosmosView.rating = currentPlace.rating
        }
    }
    
    private func setupNavigationBar() {
        
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil) //clear "name" near back btn on navController
        }
        
        navigationItem.leftBarButtonItem = nil
        title = currentPlace?.name
        saveBtn.isEnabled = true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier, let mapVC = segue.destination as? MapViewController else { return }
        
        mapVC.incomeSegueIdentifier = identifier
        mapVC.mapViewControllerDelegate = self
        
        switch identifier {
        case "showPlace":
            mapVC.place.name = placeName.text!
            mapVC.place.location = placeLocation.text
            mapVC.place.type = placeType.text
            mapVC.place.imageData = placeImage.image?.pngData()
            break
        case "getAddress":
            break
        default:
            break
        }
    }
}

//MARK: Work with TextFieldDelegate
extension NewPlaceViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    @objc private func textFiledChanged() {
        if placeName.text?.trimmingCharacters(in: .whitespaces).isEmpty == false {
            saveBtn.isEnabled = true
        } else {
            saveBtn.isEnabled = false
        }
    }
}

//MARK: Work with image
extension NewPlaceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func chooseImagePicker(source: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let pickerController = UIImagePickerController()
            
            pickerController.delegate = self
            pickerController.allowsEditing = true
            pickerController.sourceType = source
            
            present(pickerController, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        placeImage.image = info[.editedImage] as? UIImage
        placeImage.contentMode = .scaleAspectFill
        placeImage.clipsToBounds = true
        
        imageIsChanged = true
        
        dismiss(animated: true)
    }
}

//MARK: MapViewControllerDelegate
extension NewPlaceViewController: MapViewControllerDelegate {
    func getAddress(_ adrress: String?) {
        placeLocation.text = adrress
    }
}
