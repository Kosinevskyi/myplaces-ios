//
//  CustomTableViewCell.swift
//  MyPlaces
//
//  Created by Roman Kosinevskyi on 02.01.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cosmos

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var imageOfPlace: UIImageView! {
        didSet {
            imageOfPlace.layer.cornerRadius = imageOfPlace.frame.size.height / 2 //for circle view
            imageOfPlace.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameL: UILabel!
    @IBOutlet weak var locationL: UILabel!
    @IBOutlet weak var typeL: UILabel!
    @IBOutlet weak var cosmosView: CosmosView! {
        didSet {
            cosmosView.settings.updateOnTouch = false
        }
    }
    
    func consigureCell(place: Place) {
        imageOfPlace.image = UIImage(data: place.imageData!)
        nameL.text = place.name
        locationL.text = place.location
        typeL.text = place.type
        cosmosView.rating = place.rating
    }
}
