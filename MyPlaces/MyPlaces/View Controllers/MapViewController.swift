//
//  MapViewController.swift
//  MyPlaces
//
//  Created by Roman Kosinevskyi on 06.01.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol MapViewControllerDelegate {
    func getAddress(_ adrress: String?)
}

class MapViewController: UIViewController {
    
    //MARK: public
    let mapManager = MapManager()
    var place = Place()
    var incomeSegueIdentifier = ""
    var mapViewControllerDelegate: MapViewControllerDelegate?
    
    //MARK: private
    private let annotationIdentifier = "annotationIdentifier"
    
    private var previousLocation: CLLocation? {
        didSet {
            mapManager.startTrakingUserLocation(for: mapView, and: previousLocation) { (currentLocation) in
                self.previousLocation = currentLocation
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.mapManager.showUserLocation(mapView: self.mapView)
                }
            }
        }
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapPinImage: UIImageView!
    @IBOutlet weak var addressL: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var goBtn: UIButton!
    
    //MARK: IBAction
    @IBAction func centerViewInUserLocation() {
        mapManager.showUserLocation(mapView: mapView)
    }
    
    @IBAction func goBtnPressed() {
        mapManager.getDirections(for: mapView) { (location) in
            self.previousLocation = location
        }
    }
    
    @IBAction func doneBtnPressed() {
        mapViewControllerDelegate?.getAddress(addressL.text)
        closeAction()
    }
    
    //MARK: override func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressL.text = ""
        mapView.delegate = self
        setupMapView()
    }
    
    @IBAction func closeAction() {
        dismiss(animated: true)
    }
    
    //MARK: private func
    private func setupMapView() {
        
        mapManager.checkLocationServices(mapView: mapView, segueIdentifier: incomeSegueIdentifier) {
            mapManager.locationManager.delegate = self
        }
        
        if incomeSegueIdentifier == "showPlace" {
            hideElementsForFindingAddress()
            mapManager.setupPlacemark(place: place, mapView: mapView)
        } else if incomeSegueIdentifier == "getAddress" {
            goBtn.isHidden = true
        }
    }
    
    private func hideElementsForFindingAddress() {
        mapPinImage.isHidden = true
        addressL.isHidden = true
        doneBtn.isHidden = true
    }
    
    //MARK: locationManager.startUpdatingLocation()
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if incomeSegueIdentifier == "showPlace" && previousLocation != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.mapManager.showUserLocation(mapView: self.mapView)
            }
        }
    }
}

//MARK: MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !(annotation is MKUserLocation) else { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.canShowCallout = true
        }
        
        if let imageData = place.imageData {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            imageView.layer.cornerRadius = 10
            imageView.clipsToBounds = true
            imageView.image = UIImage(data: imageData)
            annotationView?.rightCalloutAccessoryView = imageView
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = mapManager.getCenterLocation(for: mapView)
        let geocoder = CLGeocoder()
        
//        if incomeSegueIdentifier == "showPlace" && previousLocation != nil {
//            print("if incomeSegueIdentifier == showPlace && previousLocation != nil")
////            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
////                self.showUserLocation()
////            }
//        }

        geocoder.cancelGeocode()
        
        geocoder.reverseGeocodeLocation(center) { (placemarks, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let placemarks = placemarks else { return }
            
            let placemark = placemarks.first
            let streetName = placemark?.thoroughfare
            let buildNumber = placemark?.subThoroughfare
            
            DispatchQueue.main.async {
                var text = ""
                
                if streetName != nil && buildNumber != nil {
                    text = "\(streetName!), \(buildNumber!)"
                } else if streetName != nil {
                    text = "\(streetName!)"
                }
                
                self.addressL.text = text
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        
        renderer.strokeColor = .blue
        
        return renderer
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapManager.checkLocationAuthorization(mapView: mapView, segueIdentifier: incomeSegueIdentifier)
    }
}
