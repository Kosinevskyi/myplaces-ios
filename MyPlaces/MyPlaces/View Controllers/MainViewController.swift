//
//  MainViewController.swift
//  MyPlaces
//
//  Created by Roman Kosinevskyi on 01.01.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

//import UIKit
import RealmSwift

class MainViewController: UIViewController {
    
    //MARK: public
    
    //MARK: private
    private var ascendingSorting = true
    private let searchController = UISearchController(searchResultsController: nil)
    private var places: Results<Place>!
    private var filteredPlaces: Results<Place>!
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reversedSortingBtn: UIBarButtonItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //MARK: IBAction
    @IBAction func unwindSegue(_ segue: UIStoryboardSegue) {
        if segue.identifier == "showDetail" {
            print("if segue.identifier == showDetail")
        }
        
        guard let newPlaceVC = segue.source as? NewPlaceViewController else { return }
        newPlaceVC.savePlace()
        tableView.reloadData()
    }
    
    @IBAction func sortSelection(_ sender: UISegmentedControl) {
        
        sorting()
    }
    
    @IBAction func reversedSorting(_ sender: Any) {
        
        ascendingSorting.toggle()
        
        if ascendingSorting {
            reversedSortingBtn.image = UIImage(named: "AZ")
        } else {
            reversedSortingBtn.image = UIImage(named: "ZA")
        }
        
       sorting()
    }
    //MARK: func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        places = realm.objects(Place.self)
        
        setupSearchController()
        
        CloudManager.fetchDataFromCloud(places: places) { (place) in
            StorageManager.saveObject(place)
            self.tableView.reloadData()
            
            CloudManager.getImageFromCloud(place: place) { (imageData) in
                try! realm.write {
                    place.imageData = imageData
                }
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: private methods
    private func sorting() {
        if segmentedControl.selectedSegmentIndex == 0 {
            places = places.sorted(byKeyPath: "date", ascending: ascendingSorting)
        } else {
            places = places.sorted(byKeyPath: "name", ascending: ascendingSorting)
        }
        
        tableView.reloadData()
    }
    
    private func showAlert(title: String, message: String, closure: @escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAlert = UIAlertAction(title: "Delete", style: .destructive) { (_) in
            closure()
        }
        let cancelAlert = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(okAlert)
        alert.addAction(cancelAlert)
        
        present(alert, animated: true)
    }
    
    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.isActive = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
     // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            
            guard let indexPath = tableView.indexPathForSelectedRow else { return }

            let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]
            
            let newPlaceVC = segue.destination as! NewPlaceViewController
            newPlaceVC.currentPlace = place
        }
     }
}

//MARK: UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredPlaces.count
        }
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell

        let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]

        cell.consigureCell(place: place)

        return cell
    }
}

//MARK: TableViewDelegate
extension MainViewController: UITableViewDelegate {
        
    //    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    //
    //        let place = places[indexPath.row]
    //        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
    //            StorageManager.deleteObject(place)
    //            tableView.deleteRows(at: [indexPath], with: .automatic)
    //        }
    //
    //        return [deleteAction]
    //    }
        
     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let place = places[indexPath.row]
        let contextItem = UIContextualAction(style: .destructive, title: "delete") {  (_, _, _) in
            
            self.showAlert(title: "To delete a record?", message: "This record will be deleted from all your devices!") {
                CloudManager.deleteRecord(recordID: place.recordID)
                StorageManager.deleteObject(place)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem])

        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        filteredPlaces = places.filter("name CONTAINS[c] %@ OR location CONTAINS[c] %@", searchText, searchText)
        
        tableView.reloadData()
    }
}
